#! /usr/bin/env bash
set -e

echo -e "\n--------------\n" `git log --oneline | wc -l` "commits!\n--------------\n";
