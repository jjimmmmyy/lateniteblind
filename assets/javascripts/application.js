$(document).ready(function()
{
//-----------------------------------------------------------------------------
// Select subtotal item price:
// Items index
//----------------------------
    $('.item_subtotal_check input[type=checkbox]').change(subTotal);
    function subTotal()
    {
        var total = 0.0;
        $.each($('.item_subtotal_check input:checked'), function()
        {
            var value = parseFloat(this.id);
            total += value;
        });

        // set the total price
        $('.total_price_value').text("$ " + total.toFixed(2));
    };

    $('#select_all').change(toggleAllChecked);
    function toggleAllChecked()
    {
        if (this.checked)
        {
            $('#select_all').prop('checked', true);
            $('.item_subtotal_check input').each(function()
            {
                $(this).prop('checked', true);
            });
        }
        else
        {
            $('#select_all').prop('checked', false);
            $('.item_subtotal_check input:checked').each(function()
            {
                $(this).prop('checked', false);
            });
        }
        subTotal();
    }
//-----------------------------------------------------------------------------
// Validation for:
// Signup (create new user form)
//------------------------------
    var signupForm = $('#signup-form'); 
    var email = $('#email');
    var password = $('#password');
    var confirmation = $('#password-confirmation');

    var emailErrorInfo = $('#email-error-info');
    var passwordErrorInfo = $('#password-error-info');
    var confirmationErrorInfo = $('#confirmation-error-info');
    
    signupForm.on("keyup", "#email", validateEmail);
    signupForm.on("keyup", "#password", validatePassword);
    signupForm.on("keyup", "#password-confirmation", validateConfirmation);

    email.blur(validateEmail);
    password.blur(validatePassword);
    confirmation.blur(validateConfirmation);

    signupForm.submit(function(e)
    {
        // prevent form from submitting
        e.preventDefault();
        e.stopPropagation();
        
        // apply validation
        if ( validateEmail() && validatePassword() && validateConfirmation())
        {
            $(this).off("submit");
            this.submit();
        }
    });

    function validateEmail()
    {
        var emailFilter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        if (($.trim(email.val()) === "") ||
            (! emailFilter.test(email.val())))
        {
            email.addClass("error");
            emailErrorInfo.addClass("error");
            emailErrorInfo.text("invalid email address");
            return false;
        }
        else
        {
            email.removeClass("error");
            emailErrorInfo.removeClass("error");
            emailErrorInfo.text("");
            return true;
        }
    }
    
    function passwordFilter(regexFilter, passwordString)
    {
        return !regexFilter.test(passwordString);
    }

    function validatePassword()
    {
        var lowercaseFilter = /[a-z]{1,}/;
        var uppercaseFilter = /[A-Z]{1,}/;
        var digitFilter = /[0-9]{1,}/;
        
        // test for existence
        if ($.trim(password.val()) === "")
        {
            password.addClass("error");
            passwordErrorInfo.addClass("error");
            passwordErrorInfo.text("required");
            return false;
        }
        // test against regex 
        else if (passwordFilter(lowercaseFilter, $.trim(password.val()))) 
        {
            password.addClass("error");
            passwordErrorInfo.addClass("error");
            passwordErrorInfo.text("must contain at least one (1) lowercase character");
            return false;
        }
        else if (passwordFilter(uppercaseFilter, $.trim(password.val()))) 
        {
            password.addClass("error");
            passwordErrorInfo.addClass("error");
            passwordErrorInfo.text("must contain at least one (1) uppercase character");
            return false;
        }
        else if (passwordFilter(digitFilter, $.trim(password.val()))) 
        {
            password.addClass("error");
            passwordErrorInfo.addClass("error");
            passwordErrorInfo.text("must contain at least one (1) digit character");
            return false;
        }
        // test length
        else if (password.val().length < 5)
        {
            password.addClass("error");
            passwordErrorInfo.addClass("error");
            passwordErrorInfo.text("must be at least 5 characters");
            return false;
        }
        else
        {
            password.removeClass("error");
            passwordErrorInfo.removeClass("error");
            passwordErrorInfo.text("");
            return true;
        }
    }

    function validateConfirmation()
    {
        // check that confirmation is equal to password
        if ($.trim(confirmation.val()) === "")
        {
            return false;
        }
        else if (password.val() != confirmation.val())
        {
            confirmation.addClass("error");
            confirmationErrorInfo.addClass("error");
            confirmationErrorInfo.text("Passwords don't match!");
            return false;
        }
        else
        {
            confirmation.removeClass("error");
            confirmationErrorInfo.removeClass("error");
            confirmationErrorInfo.text("");
            return true;
        }
    }

//-----------------------------------------------------------------------------
// Validation for:
// Add new item to wishlist (create new item form) &
// Edit existing item in wishlist (edit item form)
//------------------------------------------------
    var itemForm = $('#item-form');
    var itemName = $('#item-name');
    var itemLink = $('#item-link');
    var itemPrice = $('#item-price');
    
    var itemNameErrorInfo = $('#item-name-error-info');
    var itemLinkErrorInfo = $('#item-link-error-info');
    var itemPriceErrorInfo = $('#item-price-error-info');
    
    itemForm.on("keyup", "#item-name", validateItemName);
    itemForm.on("keyup", "#item-link", validateItemLink);
    itemForm.on("keyup", "#item-price", validateItemPrice);

    itemName.blur(validateItemName);
    itemLink.blur(validateItemLink);
    itemPrice.blur(validateItemPrice);

    itemForm.submit(function(e)
    {
        // prevent form from submitting
        e.preventDefault();
        e.stopPropagation();
        
        // apply validation
        if ( validateItemName() && validateItemPrice() )
        {
            $(this).off("submit");
            this.submit();
        }
    });

    function validateItemName()
    {
        if ($.trim(itemName.val()) === "") 
        {
            itemName.addClass("error");
            itemNameErrorInfo.addClass("error");
            itemNameErrorInfo.text("required");
            return false;
        }
        else
        {
            itemName.removeClass("error");
            itemNameErrorInfo.removeClass("error");
            itemNameErrorInfo.text("");
            return true;
        }
    }
    
    function validateItemPrice()
    {
        var priceDecimalFilter = /^[0-9]{0,4}\.[0-9]{0,2}$/;
        var priceShortFilter = /^[0-9]{0,4}$/;
        var aValidPrice =   priceDecimalFilter.test(itemPrice.val()) ||
                            priceShortFilter.test(itemPrice.val());
        if ( ($.trim(itemPrice.val()) === "") || (! aValidPrice) ) 
        {
            itemPrice.addClass("error");
            itemPriceErrorInfo.addClass("error");
            itemPriceErrorInfo.text("invalid price");
            return false;
        }
        else
        {
            itemPrice.removeClass("error");
            itemPriceErrorInfo.removeClass("error");
            itemPriceErrorInfo.text("");
            return true;
        }
    }
});
