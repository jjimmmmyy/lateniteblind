<div class='center push_down'>
    <h1>Users</h1> 
    <ul>
        <?php 
        foreach ($users as $user)
        { ?>
            <li>
                <!-- link_to user_path(id) -->
                <?php echo '<a href="'.REDIRECT_PATH.'users/'.$user->id.'">'; ?>
                <?php echo $user->email; ?></a>
            </li>
        <?php
        } ?>
    </ul>

    <?php echo '<a href="'.REDIRECT_PATH.'users/new">'; ?>Sign up</a>
    <?php echo '<a href="'.REDIRECT_PATH.'login">'; ?>Log in</a>
</div>
