<div class='sign_up grid_4 prefix_4 suffix_4'>
    <h2>Create your account</h2>
    <?php echo '<form action="' . REDIRECT_PATH . 'signup" 
                    id="signup-form" method="post">'; ?>
        <p class='field'>
            <input type="text" name="email" id="email" 
                placeholder="email address">
            <span class='error-info' id='email-error-info'></span>
        </p>
        <p class='field'>
            <input type="password" name="password" id="password"
                placeholder="password">
            <span class='error-info' id='password-error-info'></span>
        </p>
        <p class='field'>
            <input type="password" name="password-confirmation" 
                    id="password-confirmation" placeholder="confirmation">
            <span class='error-info' id='confirmation-error-info'></span>
        </p>
        <p class='field'>
            <input type="hidden" name="create-user">
            <input type="submit" value="Sign up" class="button dark_button" 
                id="sign_in_button">
        </p>
    </form>
</div>
<div class='grid_6 prefix_3 suffix_3'>
    <div class='login_link'>
        Already have an account?
        <?php echo '<a href="' . LOGIN_PATH . '">'; ?>Log in now</a> 
    </div>
</div>
