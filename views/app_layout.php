<!DOCTYPE html>
<html>
    <head>
        <?php echo '<link rel="stylesheet" type="text/css" href="'
        . REDIRECT_PATH . 'assets/stylesheets/third_party/reset.css"/>'; ?>
        <?php echo '<link rel="stylesheet" type="text/css" href="'
        . REDIRECT_PATH . 'assets/stylesheets/third_party/960_grid.css"/>'; ?>
        <?php echo '<link rel="stylesheet" type="text/css" href="'
        . REDIRECT_PATH . 'assets/stylesheets/app.css"/>'; ?>
        <title>Window|Shopper</title>
    </head>
    <body>
        <div id="super_container">
        <div class="container_12" id="menu_top">
            <div class="grid_4">
                <h1 id="site_name">Window|Shopper</h1>
            </div>
            <div class="grid_4">
                <?php echo '<a href="' . REDIRECT_PATH . 'wishlists' . '">'; ?>
                <div id="wishlist_button" 
                    class="button light_button">Wishlists</div>
                </a>
            </div>
            <div class="grid_2 prefix_2">
            <?php echo '<a href="';
                if ($loggedIn) 
                {
                    // show logged in menu
                    echo LOGOUT_PATH . '">'; ?>
                    <div id="logout_button" 
                        class="button light_button">Logout</div>
                    </a>
                <?php
                }
                else
                {
                    // show logged out menu
                    echo LOGIN_PATH . '">'; ?>
                    <div id="login_button" 
                        class="button light_button">Login</div>
                    </a>
            <?php
                }
            ?>
            </div>
        </div>
        
        <div class="container_12" id="container">
            <?php include_once $viewPath; ?>
        </div>

        <div class="container_12" id="footer">James Jackson 2014</div>
        </div>
         
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script> 
        <?php echo '<script src="' . REDIRECT_PATH . 
                    'assets/javascripts/application.js' . '"></script>';?>
    </body>
</html>
