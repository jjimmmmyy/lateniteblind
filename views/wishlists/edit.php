<div class='grid_8 prefix_2 suffix_2' id='edit_wishlist_form'>
    <h2>Edit this wishlist</h2>
    
    <?php echo '<form action="' . REDIRECT_PATH . 
                'wishlists" id="wishlist-form" method="post" >'; ?>
        <div class='field'>
            <?php echo '<input type="text" name="name" id="wishlist-name" value="' 
                . $wishlist->name . '" placeholder="Wishlist name">'; ?>
            <span class='error-info' id="wishlist-name-error-info"></span>
        </div>
        
        <div class='container_12 no_margin_left'>
            <div class='grid_4'>
                <div class='back_link'>
                    <?php echo '<a href="' . REDIRECT_PATH .
                                'wishlists/">'; ?>Back</a> to your other wishlists
                </div>
            </div>
            <div class='grid_4 field'>
                <?php echo '<input type="hidden" name="id" value="' .
                            $wishlist->id . '">'; ?>
                <input type="hidden" name="update_wishlist">
                <input type="submit" id="submit-update-wishlist" 
                class="submit-button light_button" value="Update this wishlist">
            </div>
        </div>
        </p>
    </form>
    
</div>
