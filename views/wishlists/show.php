<div class='wishlist_header'>
    <div class='grid_6'>
        <h1 id='list_title'>My wishlist</h1>
    </div>
    <div id='total_price' class='grid_4'> 
        <div class='total_price_label'>Total price</div>
        <div class='total_price_value'>$ <?php echo $total_price; ?></div>
    </div>
    <div class='grid_2 fixed' id="add_item_button">
    <?php echo '<a href="'.REDIRECT_PATH.'items/new">'; ?>
        <h1 class='button light_button'>+</h1></a>
    </div>
</div>
<div class='minor_header'>
    <div class='grid_7'>Item name</div>
    <div class='grid_1'>Qty</div>
    <div class='grid_2'>Price</div>
</div>

<ul>
    <?php 
    foreach ($items as $item)
    { ?>
    <li class='item_short'>
        <?php if ($item->link !== "")
        {
            echo '<a href="'.$item->link.'" target="_blank">'; 
        } ?>
        
        <div class='item_name grid_5'><?php echo $item->name; ?></div>
        
        <?php if ($item->link !== "") 
        {
            echo '</a>';
        } ?>
       
        <?php echo '<a href="'.REDIRECT_PATH.'items/'.$item->id.'/edit">'; ?>
        <div class='edit_link grid_1 button'>Edit</div></a>
        
        <?php echo '<a href="'.REDIRECT_PATH.'items/'.$item->id.'/delete">'; ?>
        <div class='delete_link grid_1 button'>Delete</div></a>

        <div><div class='item_qty grid_1'>1</div></div>

        <div><div class='item_price grid_2'>
            <div class="dollar_sign">$</div>
            <div class="price"><?php echo $item->price; ?></div>
        </div></div>
    </li>
    <?php
    } ?>
</ul>
