<div class='grid_8 prefix_2 suffix_2' id='new_wishlist_form'>
    <h2>Create a new wishlist</h2>

    <?php echo '<form action="' . REDIRECT_PATH . 
                'wishlists" id="wishlist-form" method="post">'; ?>
        <div class='field'>
            <input type="text" name="name" id="wishlist-name"
                    placeholder="Wishlist name">
            <span class='error-info' id="wishlist-name-error-info"></span>
        </div>
        
        <div class='container_12 no_margin_left'>
            <div class='grid_4'>
                <div class='back_link'>
                    <?php echo '<a href="'.REDIRECT_PATH.'wishlists/">'; ?>
                    Back to your other wishlists</a>
                </div>
            </div>
            <div class='grid_4 field'>
                <input type="hidden" name="create_wishlist">
                <input type="submit" id="submit-create-wishlist" 
                        class="submit-button light_button" value="Add new wishlist">
            </div>
        </div>
    </form>

</div>
