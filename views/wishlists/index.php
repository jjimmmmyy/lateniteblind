<div id='wishlist_index'>
<?php
if (! $wishlists)
{ ?>
    <div class='grid_12'>
        <h2 id="no_wishlists">You have no wishlists!</h2>
    </div>
        
    <div class='grid_12' id="create_first_wishlist">
        <?php echo '<a href="' . REDIRECT_PATH . 'wishlists/new">'; ?>
            <h1 class='button light_button'>Create your first wishlist</h1>
        </a>
    </div>
<?php
}
else
{ ?>
    <div class='grid_8'>
        <?php if (count($wishlists) === 1) { ?>
            <p class='help_message'>Click on your wishlist to add items</p>
            <ul class="highlight_first_child">
        <?php } else if ($there_is_an_empty_wishlist) { ?>
            <p class='help_message'>Click on your empty wishlist to add items</p>
            <ul>
        <?php } ?> 
            <?php
            foreach ($wishlists as $wishlist)
            { ?>
            <?php if ( $wishlists_item_counts["{$wishlist->id}"] === 0) { ?>
            <li class='wishlist_short wishlist_empty'>
            <?php } else { ?>
            <li class='wishlist_short'>
            <?php } ?>
                <?php echo '<a href="'.REDIRECT_PATH.'wishlists/'.$wishlist->id.'">';?>
                <div class='wishlist_name grid_5'>
                    <?php echo $wishlist->name; ?>
                </div>
                </a>
                
                <?php echo '<a href="'.REDIRECT_PATH.'wishlists/'.$wishlist->id.'">';?>
                <span class='item_count grid_1'>
                    <?php echo '( '. $wishlists_item_counts["{$wishlist->id}"] . ' )'; ?>
                </span>
                </a>
                <?php echo '<a href="'.REDIRECT_PATH.'wishlists/'.
                            $wishlist->id.'/edit">'; ?>
                <div class='edit_link grid_1 button'>Edit</div></a>
                
                <?php echo '<a href="'.REDIRECT_PATH.'wishlists/'.
                            $wishlist->id.'/delete">'; ?>
                <div class='delete_link grid_1 button'>Delete</div></a>
            </li>
            <?php
            } ?>
        </ul>
    </div>
    <div class='grid_2'><br/></div>
    <div class='grid_2 fixed' id="create_another_wishlist">
        <?php echo '<a href="' . REDIRECT_PATH . 'wishlists/new">'; ?>
            <h1 class='button light_button'>+</h1>
        </a>
    </div>
<?php
}
?>
</div>
