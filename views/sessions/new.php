<div class='log_in grid_4 prefix_4 suffix_4'>
    <?php echo '<form action="'.REDIRECT_PATH .
                'login" id="login-form" method="post">'; ?>
        <p class='field'>
            <input type="text" name="email" id="email"
                placeholder="email address"/>
        </p>
        <p class='field'>
            <input type="password" name="password" id="password"
                placeholder="password"/>
        </p>
        <p class='field'>
            <input type="hidden" name="login">
            <input type="submit" value="Log in" class="button dark_button" 
                id="log_in_button">
        </p>
    </form>
</div>
<div class='grid_6 prefix_3 suffix_3'>
    <div class='signup_link'>
        Don't have an account?
        <?php echo '<a href="' . SIGNUP_PATH . '">'; ?>Sign up now</a> 
    </div>
</div>
