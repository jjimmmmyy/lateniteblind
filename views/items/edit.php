<div class='grid_8 prefix_2 suffix_2' id='edit_item_form'>
    <h2>Edit this item</h2>
    
    <?php echo '<form action="' . REDIRECT_PATH . 
                'items" id="item-form" method="post" >'; ?>
        <p class='field'>
            <div class='field_label'>Item name:</div>
            <?php echo '<input type="text" name="name" id="item-name" value="' 
                . $item->name . '">'; ?>
            <span class='error-info' id="item-name-error-info"></span>
        </p>
        
        <p class='field'>
            <div class='field_label'>Price:</div>
            <?php echo '<input type="text" name="price" id="item-price" value="' 
                . $item->price . '">';?>
            <span class='error-info' id="item-price-error-info"></span>
        </p>
    
        <p class='field'>
            <div class='field_label'>Link to the item:</div> 
            <?php echo '<input type="text" name="link" id="item-link" value="' 
                . $item->link .'">'; ?>
            <span class='error-info' id="item-link-error-info"></span>
        </p>

        <p class='field'>
            <div class='field_label'>Rating:</div> 
            <?php echo '<input type="text" name="rating" id="item-rating" value="' 
                . $item->rating . '">'; ?>
        </p>
        
        <div class='container_12 no_margin_left'>
            <div class='grid_4'>
                <div class='back_link'>
                    <?php echo '<a href="'.REDIRECT_PATH.'items/">'; ?>Back</a> 
                    to your wishlist
                </div>
            </div>
            <div class='grid_4 field'>
                <?php echo '<input type="hidden" name="id" value="' . $item->id . '">'; ?>
                <input type="hidden" name="update_item">
                <input type="submit" id="submit-update-item" 
                    class="submit-button light_button" value="Update this item">
            </div>
        </div>
    </form>
    
</div>
