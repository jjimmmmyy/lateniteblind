<div class='wishlist_header'>
    <div class='grid_6'>
        <h1 id='list_title'><?php echo $wishlist_name; ?></h1>
    </div>
    <div id='total_price' class='grid_4'> 
        <div class='total_price_label'>Total price</div>
        <div class='total_price_value'>$ <?php echo $total_price; ?></div>
    </div>
    <div class='grid_2 fixed' id="add_item_button">
    <?php echo '<a href="'.REDIRECT_PATH.'items/new">'; ?>
        <h1 class='button light_button'>+</h1></a>
    </div>
</div>

<?php
if (! $items)
{ ?>
    <div class='grid_10 prefix_2 no_items'>
        <h2 id="no_items">You have no items!</h2>
    </div>
        
    <div class='grid_5 prefix_7' id="add_first_item">
        <h2>Add an item with this button -></h1>
    </div>
<?php
}
else
{ ?>

<div class='minor_header'>
    <div class='grid_2'>Item name</div>
    <div class='grid_6'>Select all <input type="checkbox" id="select_all"
        checked="false"></div>
    <div class='grid_3'>Price</div>
</div>

<ul>
    <?php 
    foreach ($items as $item)
    { ?>
    <li class='item_short'>
        <?php if ($item->link !== "")
        {
            echo '<a href="'.$item->link.'" target="_blank">'; 
        } ?>
        
        <div class='item_name grid_5'><?php echo $item->name; ?></div>
        
        <?php if ($item->link !== "") 
        {
            echo '</a>';
        } ?>

        <div class='item_subtotal_check grid_1'>
            <input type="checkbox" <?php echo 'id="' . $item->price . '"'; ?>
            checked="false">
        </div>
       
        <?php echo '<a href="'.REDIRECT_PATH.'items/'.$item->id.'/edit">'; ?>
        <div class='edit_link grid_1 button'>Edit</div></a>
        
        <?php echo '<a href="'.REDIRECT_PATH.'items/'.$item->id.'/delete">'; ?>
        <div class='delete_link grid_1 button'>Delete</div></a>

        <div><div class='item_price grid_2'>
            <div class="dollar_sign">$</div>
            <div class="price"><?php echo $item->price; ?></div>
        </div></div>
    </li>
    <?php
    } ?>
</ul>
<?php
}
?>
