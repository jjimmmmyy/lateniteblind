<div class='grid_8 prefix_2 suffix_2' id='new_item_form'>
    <h2>Add a new item to your wishlist</h2>

    <?php echo '<form action="' . REDIRECT_PATH . 
                'items" id="item-form" method="post" >'; ?>
        <p class='field'>
            <div class='field_label'>Item name:</div>
            <input type="text" name="name" id="item-name">
            <span class='error-info' id="item-name-error-info"></span>
        </p>
        
        <p class='field'>
            <div class='field_label'>Price:</div>
            <input type="text" name="price" id="item-price">
            <span class='error-info' id="item-price-error-info"></span>
        </p>
        
        <p class='field'>
            <div class='field_label'>Link to the item:</div>
            <input type="text" name="link" id="item-link">
            <span class='error-info' id="item-link-error-info"></span>
        </p>

        <p class='field'>
            <div class='field_label'>Rating:</div>
            <input type="text" name="rating" id="item-rating">
        </p>

        <div class='container_12 no_margin_left'>
            <div class='grid_4'>
                <div class='back_link'>
                    <?php echo '<a href="'.REDIRECT_PATH.'items/">'; ?>
                    Back to your wishlist</a>
                </div>
            </div>
        <div class='grid_4 field'>
            <input type="hidden" name="create_item">
            <input type="submit" id="submit-create-item" 
                class="submit-button light_button" value="Add new item">
            </div>
        </div>
    </form>
</div>
