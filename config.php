<?php
/*  ======================================================================
    File: LateNiteBlind/config.php
    Creator: James Jackson
    ======================================================================  */

/*  ======================================================================
    Path Definitions
    ======================================================================  */
if (getenv('HTTP_HOST') === 'localhost')
{
    define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . '/LateNiteBlind/'); 
    define('REDIRECT_PATH', 'http://localhost/~Jimmy/LateNiteBlind/');
}
else
{
    // redefine (on heroku server)
    define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . '/');
    define('REDIRECT_PATH', 'http://window-shopper-0.herokuapp.com/');
}

// routes
define('LOGIN_PATH', REDIRECT_PATH . 'login');
define('LOGOUT_PATH', REDIRECT_PATH . 'logout');
define('SIGNUP_PATH', REDIRECT_PATH . 'signup');
?>
