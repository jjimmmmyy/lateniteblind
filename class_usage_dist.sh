#! /usr/bin/env bash
set -e

if [ "$1" = "--within-file" ]
then
    order_by_per_file=0
else
    order_by_per_file=1
fi

if [ $order_by_per_file == 1 ]
then 
    echo -e "/file\tfile/\tfile name\n--------------------------------------";
else
    echo -e "file/\t/file\tfile name\n--------------------------------------";
fi

find . -iname '*.php' | 
xargs grep -h '^class\b' | 
sed 's/^[[:space:]]*//' | 
cut -d ' ' -f 2 | 
while read class; do 
    if [ $order_by_per_file == 1 ]
    then 
        echo -e `grep -rl "\b$class\b" . --include="*.php" | wc -l` "\t" `grep -r "\b$class\b" . --include="*.php" | wc -l` "\t" $class; 
    else
        echo -e `grep -r "\b$class\b" . --include="*.php" | wc -l` "\t" `grep -rl "\b$class\b" . --include="*.php" | wc -l` "\t" $class; 
    fi
    done |
sort -n
