<?php
/*  ======================================================================
    File: LateNiteBlind/controllers/users_controller.php
    Creator: James Jackson
    ======================================================================  */
 
include_once '../config.php';
include_once ROOT_PATH . 'models/user.php';
include_once ROOT_PATH . 'controllers/app_controller.php';
include_once ROOT_PATH . 'controllers/sessions_controller.php';

class UsersController extends AppController
{  
/*  **********************************************************************
    Function: render
    Description: Renders a view (see AppController for more details)
    **********************************************************************  */
    protected function render($viewFile, $variables = array())
    {
        $viewPath = ROOT_PATH . 'views/users/' . $viewFile;
        parent::render($viewPath, $variables);
    }

/*  **********************************************************************
    Function: newForm
    Description: Renders the signup form view for adding a new User.
    **********************************************************************  */
    public function newForm()
    {
        $user = new User();

        UsersController::render('new.php', array('user' => $user));
    }

/*  **********************************************************************
    Function: create
    Description: Signs up the new User (saves them to the db),
                 and signs the User in
    **********************************************************************  */
    public function create($email, $password)
    {
        $user = new User(null, $email, $password);
        $user->save();
        
        // sign user in
        SessionsController::create($email, $password);
    }

/*  **********************************************************************
    Function: parametersAreValid
    Description: Validates $_GET and $_POST variables to allow safe routing
    **********************************************************************  */
    public static function parametersAreValid($params = array())
    {
        foreach ($params as $key => $value)
        {
            switch ($key)
            {
                case 'email':
                    // email, required
                    if ($value === "" ||
    preg_match("/^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/", $value) !== 1)
                    {
                        return false;
                    }
                    break;
                case 'password':
                    // password, required
                    if ($value === "" || strlen($value) < 5 ||
                        preg_match("/[a-z]{0,}/", $value) !== 1 || 
                        preg_match("/[A-Z]{0,}/", $value) !== 1 || 
                        preg_match("/[0-9]{0,}/", $value) !== 1)  
                    {
                        return false;
                    }
                    break;
                default:
                    // value is invalid (parameter not in whitelist)
                    return false;
            }
        }
        
        return true;
    }
}

/*  ======================================================================
    Routing                 (& white listing TODO or black listing ? )
    ======================================================================  */
// create a new user
if (isset($_POST['create-user']) && isset($_POST['email']))
{
    // validate user input
    if (UsersController::parametersAreValid(array(
        'email' => $_POST['email'],
        'password' => $_POST['password'])))
    {
        UsersController::create($_POST['email'], $_POST['password']);
    }
    else
    {
        // somehow tell the user what they did wrong (if js is disabled)
        UsersController::newForm();
    }
}
// form for a new user
else if (isset($_GET['new-user']))
{
    UsersController::newForm();
}
?>
