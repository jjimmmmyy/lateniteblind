<?php
/*  ======================================================================
    File: LateNiteBlind/controllers/app_controller.php
    Creator: James Jackson
    ======================================================================  */

include_once ROOT_PATH . 'models/user.php';

class AppController
{
/*  **********************************************************************
    Function: loggedIn
    Description: checks if the User is still logged-in
    Returns: logged-in status bool
    **********************************************************************  */
    public static function loggedIn()
    {
        return isset($_SESSION['currentUser']);
    }

/*  **********************************************************************
    Function: render
    Description: Renders a view (with default master layout), 
                 at the specified view path. Exposes variables to view.
                 Called at the end of most subclass controller actions, 
                 from the overridden version of this "render" method.
    **********************************************************************  */
    protected function render($viewPath, $variables = array())
    {
        // update the logged in status
        $loggedIn = AppController::loggedIn();
        
        // expose controller variables to view
        extract($variables);
        
        // render the view
        ob_start();
        include ROOT_PATH . 'views/app_layout.php';
        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
        
        exit(); // to prevent further output
    }

/*  **********************************************************************
    Function: authenticate
    Description: Begins a new session. 
    Returns: the current User, or redirects to login path
    **********************************************************************  */
    public static function authenticate()
    {
        if (!isset($_SESSION))
        {
            session_start();
        }
        if (isset($_SESSION['currentUser'])) 
        {
            $currentUser = User::find($_SESSION['currentUser']);
            if ($currentUser)
            {
                return $currentUser;
            }
        }

        ob_start();
        header('Location: ' . REDIRECT_PATH . 'login');
        ob_end_clean();
    }
}
?>
