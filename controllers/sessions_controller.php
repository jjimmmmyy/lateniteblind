<?php
/*  ======================================================================
    File: LateNiteBlind/controllers/sessions_controller.php
    Creator: James Jackson
    ======================================================================  */

include_once '../config.php';
include_once ROOT_PATH . 'models/user.php';
include_once ROOT_PATH . 'controllers/app_controller.php';

class SessionsController extends AppController
{
/*  **********************************************************************
    Function: render
    Description: Renders a view (see AppController for more details)
    **********************************************************************  */
    protected function render($viewFile, $variables = array())
    {
        $viewPath = ROOT_PATH . 'views/sessions/' . $viewFile;
        parent::render($viewPath, $variables);
    }

/*  **********************************************************************
    Function: newForm
    Description: Renders the login form view
    **********************************************************************  */
    public function newForm()
    {
        SessionsController::render('new.php');
    }

/*  **********************************************************************
    Function: create
    Description: Logs in a User, or reloads the login form view
    **********************************************************************  */
    public function create($email, $password)
    {
        $user = User::authenticate($email, $password);
        if (!is_null($user))
        {
            session_start();
            $_SESSION['currentUser'] = $user->id;
            
            ob_start();
            // redirect to User's Wishlists
            header('Location: ' . REDIRECT_PATH . 'wishlists');
            ob_end_clean();
        }
        else
        {
            SessionsController::render('new.php');
        }
    }

/*  **********************************************************************
    Function: destroy
    Description: Logs out the User, and redirects to the login path
    **********************************************************************  */
    public function destroy()
    {
        $_SESSION = array();
        if (ini_get("session.use_cookies"))
        {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params['path'],
                $params['domain'], $params['secure'], $params['httponly']);
        }
        session_destroy(); 
        
        ob_start();
        header('Location: ' . LOGIN_PATH);
        ob_end_clean();
    }
}

// TODO: Use a routing class instead of this logic
//       at the bottom of each controller messing with controller includes

// create a new session
if (isset($_POST['login']) && isset($_POST['email']))
{
    SessionsController::create($_POST['email'], $_POST['password']);
}
// logout 
else if (isset($_GET['logout']))
{
    SessionsController::destroy();
}
// login form
else if (isset($_GET['login-form']))
{
    SessionsController::newForm();
}
?>
