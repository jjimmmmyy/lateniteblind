<?php
/*  ======================================================================
    File: LateNiteBlind/controllers/wishlists_controller.php
    Creator: James Jackson
    ======================================================================  */

include_once '../config.php';
include_once ROOT_PATH . 'models/wishlist.php';
include_once ROOT_PATH . 'models/user.php';
include_once ROOT_PATH . 'controllers/items_controller.php';
include_once ROOT_PATH . 'controllers/app_controller.php';

class WishlistsController extends AppController
{
/*  **********************************************************************
    Function: render
    Description: Renders a view (see AppController for more details)
    **********************************************************************  */
    protected function render($viewFile, $variables = array())
    {
        $viewPath = ROOT_PATH . 'views/wishlists/' . $viewFile;
        parent::render($viewPath, $variables);
    }

/*  **********************************************************************
    Function: index
    Description: Shows all this User's Wishlists.
    **********************************************************************  */
    public function index()
    {
        $currentUser = parent::authenticate();
        $wishlists = $currentUser->allWishlists();
        
        $there_is_an_empty_wishlist = false;
        $wishlists_item_counts = array();
        foreach ($wishlists as $wishlist)
        {
            $wishlists_item_counts["{$wishlist->id}"] = $wishlist->itemCount();
            
            if ($wishlists_item_counts["{$wishlist->id}"] === 0)
            {
                $there_is_an_empty_wishlist = true;
            }
        }
        WishlistsController::render('index.php', 
                                    array('wishlists' => $wishlists,
                        'wishlists_item_counts' => $wishlists_item_counts,
                    'there_is_an_empty_wishlist' => $there_is_an_empty_wishlist));
    }

/*  **********************************************************************
    Function: index
    Description: Shows all of items in this Wishlist.
    **********************************************************************  */
    public function show($wishlist_id)
    {
        $currentUser = parent::authenticate();
        if ( $currentUser->findWishlist($wishlist_id) )
        {
            $_SESSION['currentWishlist'] = $wishlist_id;
            ItemsController::index();
        }
        else
        {
            // redirect to index if user tried to access other user's wishlist
            WishlistsController::index();
            // TODO should probably redirect to homepage instead
        }
    }

/*  **********************************************************************
    Function: newForm
    Description: Renders the "new Wishlist" form view for adding Wishlists.
    **********************************************************************  */
    public function newForm()
    {
        parent::authenticate();
        $wishlist = new Wishlist();

        WishlistsController::render('new.php', array('wishlist' => $wishlist));
    }

/*  **********************************************************************
    Function: create
    Description: Saves the new Wishlist to the db if the User is authorized.
                 Redirects to the list of all the User's Wishlists
    **********************************************************************  */
    public function create($name)
    {
        $currentUser = parent::authenticate();
        
        $wishlist = new Wishlist(null, $name, $currentUser->id);
        $wishlist->save();

        // TODO: should probably be ::show(last Created Wishlist ID); instead
        WishlistsController::index();
    }

/*  **********************************************************************
    Function: editForm
    Description: Renders the "edit Wishlist" form view for this Wishlist, 
                 or redirects to the list of Wishlists if not found
    **********************************************************************  */
    public function editForm($wishlist_id)
    {
        $currentUser = parent::authenticate();
        $wishlist = $currentUser->findWishlist($wishlist_id);
        if ($wishlist)
        {
            WishlistsController::render('edit.php', 
                                        array('wishlist' => $wishlist));
        }
        else
        {
            WishlistsController::index();
            // TODO alternatively, show the 
            // "the wishlist you were looking for could not be found :(" page
        }
    }

/*  **********************************************************************
    Function: update
    Description: Updates the Wishlist to the db if the User is authorized.
                 Redirects to the list of all the User's Wishlists
    **********************************************************************  */
    public function update($name, $id)
    {
        $currentUser = parent::authenticate();
         
        $wishlist = new Wishlist($id, $name, $currentUser->id);
        $wishlist->update(array('wishlist' => $wishlist));
        
        WishlistsController::index();
    }

/*  **********************************************************************
    Function: destroy
    Description: Deletes the Wishlist from the db if the User is authorized.
                 Redirects to the list of all the User's Wishlists
    **********************************************************************  */
    public function destroy($wishlist_id)
    {
        $currentUser = parent::authenticate();
        $wishlist = $currentUser->findWishlist($wishlist_id);
        if ($wishlist)
        {
            $wishlist->destroy();

            // TODO: flash message 'wishlist was successfully deleted'
            WishlistsController::index();
        }
        else
        {
            WishlistsController::index();    
        }
    }

/*  **********************************************************************
    Function: parametersAreValid
    Description: Validates $_GET and $_POST variables to allow safe routing
    **********************************************************************  */
    public static function parametersAreValid($params = array())
    {
        foreach ($params as $key => $value)
        {
            switch ($key)
            {
                case 'id':
                    // integer, required
                    if (! is_numeric($value))
                    {
                        return false;
                    }
                    break;
                case 'name':
                    // string, required
                    if ($value === "")
                    {
                        return false;
                    }
                    break;
                default:
                    // value is invalid (parameter not in whitelist)
                    return false;
            }
        }

        return true;
    }
    
    // check that an int won't overflow
    public static function intIsJustRight($integerValue)
    {
        return ($integerValue < PHP_INT_MAX) && ($integerValue >= 0);
    }
}

/*  ======================================================================
    Routing                 (& white listing TODO or black listing ? )
    ======================================================================  */
// form for editing a single wishlist
if (isset($_GET['edit']) && isset($_GET['wishlist_id']))
{
    if (WishlistsController::parametersAreValid(array(
        'id' => $_GET['wishlist_id'])))
    {
        if (WishlistsController::intIsJustRight($_GET['wishlist_id']))
        {
            WishlistsController::editForm($_GET['wishlist_id']);
        }
        else
        {
            WishlistsController::index();
        }
    }
    else
    {
        WishlistsController::index();
    }
}
// delete an existing wishlist
else if (isset($_GET['delete']) && isset($_GET['wishlist_id']))
{
    if (WishlistsController::parametersAreValid(array(
        'id' => $_GET['wishlist_id'])))
    {
        if (WishlistsController::intIsJustRight($_GET['wishlist_id']))
        {
            WishlistsController::destroy($_GET['wishlist_id']);
        }
        else
        {
            WishlistsController::index();
        }
    }
    else
    {
        WishlistsController::index();
    }
}
// show a single existing wishlist
else if (isset($_GET['wishlist_id']))
{
    if (WishlistsController::parametersAreValid(array(
        'id' => $_GET['wishlist_id'])))
    {
        if (WishlistsController::intIsJustRight($_GET['wishlist_id']))
        {
            WishlistsController::show($_GET['wishlist_id']);
        }
        else
        {
            WishlistsController::index();
        }
    }
    else
    {
        WishlistsController::index();
    }
}
// update an existing wishlist
else if (isset($_POST['update_wishlist']))
{
    if (WishlistsController::parametersAreValid(array(
        'name'  => $_POST['name'], 
        'id'    => $_POST['id'])))
    {
        // sanitize inputs
        $cleanName = trim($_POST['name']);
        $cleanID = trim($_POST['id']);
        if (preg_match("/^\d{1,}$/", $cleanID) !== 1)
        {
            // $_POST['id'] has somehow been tampered with
            // reload clean form
            WishlistsController::editForm($_POST['id']);
        }
        
        WishlistsController::update($cleanName, $cleanID);
    }
    else
    {
        // somehow tell the user what they did wrong (if js is disabled)
        // redirect to index, because $_POST['id'] could be tainted
        WishlistsController::index();
    }
}
// create a new wishlist
else if (isset($_POST['create_wishlist']) && isset($_POST['name']))
{
    if (WishlistsController::parametersAreValid(
        array('name'  => $_POST['name'])))
    {
        // TODO: SAME AS ABOVE !!! DRY !!!
        // TODO: side-note: fat models/skinny controllers 
        // sanitize inputs
        $cleanName = trim($_POST['name']);

        WishlistsController::create($cleanName);
    }
    else
    {
        // somehow tell the user what they did wrong (if js is disabled?)
        WishlistsController::newForm();
    }
}
// form for a new wishlist
else if (isset($_GET['new_wishlist']))
{
    WishlistsController::newForm();
}
// show all wishlists
else if (isset($_GET['wishlist']))
{
    WishlistsController::index();
}
?>
