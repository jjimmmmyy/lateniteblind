<?php
/*  ======================================================================
    File: LateNiteBlind/controllers/items_controller.php
    Creator: James Jackson
    ======================================================================  */

include_once '../config.php';
include_once ROOT_PATH . 'models/wishlist.php';
include_once ROOT_PATH . 'models/user.php';
include_once ROOT_PATH . 'models/item.php';
include_once ROOT_PATH . 'controllers/app_controller.php';
include_once ROOT_PATH . 'controllers/wishlists_controller.php';

class ItemsController extends AppController
{
/*  **********************************************************************
    Function: render
    Description: Renders a view (see AppController for more details)
    **********************************************************************  */
    protected function render($viewFile, $variables = array())
    {
        $viewPath = ROOT_PATH . 'views/items/' . $viewFile;
        parent::render($viewPath, $variables);
    }
    
/*  **********************************************************************
    Function: index
    Description: Shows all of the current User's, current Wishlist's Items.
                 Redirects to all Wishlists (index) if no Wishlist is found,
                 or the current Wishlist is not set.
    **********************************************************************  */
    public function index()
    {
        $currentUser = parent::authenticate();
        
        if (isset($_SESSION['currentWishlist']))
        {
            $currentWishlist = $currentUser->findWishlist($_SESSION['currentWishlist']);
            if ($currentWishlist)
            {
                $items = $currentWishlist->allItems();
                $total_price = 0.0;
                foreach ($items as $item)
                {
                    $total_price += $item->price;
                }

                // display price as 19.90 if price actually equals 19.9
                if (preg_match("/^[0-9]{0,4}\.[0-9]{1}$/", $total_price) === 1)
                {
                    $total_price = $total_price . "0"; 
                }
                 
                ItemsController::render('index.php', array('items' => $items, 
                                                    'total_price' => $total_price,
                                        'wishlist_name' => $currentWishlist->name));
            }
            else
            {
                WishlistsController::index();
            }
        }
        else
        {
            WishlistsController::index();
        }
    }
    
/*  **********************************************************************
    Function: newForm
    Description: Renders the form view for adding Items to a Wishlist.
    **********************************************************************  */
    public function newForm()
    {
        parent::authenticate();
        $item = new Item();

        ItemsController::render('new.php', array('item' => $item));
    }

/*  **********************************************************************
    Function: create
    Description: Saves the new Item to the current Wishlist, 
                 if the User is authorized. If the Wishlist doesn't exist, 
                 redirects to the list of all the User's Wishlists
    **********************************************************************  */
    public function create($name, $link, $rating, $price)
    {
        $currentUser = parent::authenticate();
        if (isset($_SESSION['currentWishlist']))
        {
            $currentWishlist = $currentUser->findWishlist($_SESSION['currentWishlist']);
            if ($currentWishlist)
            {
                $item = new Item(null, $name, $link, $rating, $price, 
                                $currentWishlist->id);
                $item->save();
                
                ItemsController::index();
            }
            else
            {
                WishlistsController::index(); 
            }
        }  
        else
        {
            WishlistsController::index();
        }
    }

/*  **********************************************************************
    Function: editForm
    Description: Renders the "edit Item" form view for this Item, 
                 or redirects to the list of Items if not found
    **********************************************************************  */
    public function editForm($item_id)
    {
        $currentUser = parent::authenticate();
        if (isset($_SESSION['currentWishlist']))
        {
            $currentWishlist = $currentUser->findWishlist($_SESSION['currentWishlist']);
            if ($currentWishlist)
            {
                $item = $currentWishlist->findItem($item_id);
                if ($item)
                {
                    ItemsController::render('edit.php', array('item' => $item));
                }
                else
                {
                    ItemsController::index();
                    // alternatively, show the 
                    // "the item you were looking for could not be found :(" page
                }
            }
            else
            {
                WishlistsController::index();
            }
        }
        else
        {
            WishlistsController::index();
        }
    }

/*  **********************************************************************
    Function: update
    Description: Updates the Item to the db if the User is authorized.
                 Redirects to the list of all the Wishlist's Items
    **********************************************************************  */
    public function update($name, $link, $rating, $price, $id)
    {
        $currentUser = parent::authenticate();
        if (isset($_SESSION['currentWishlist']))
        {
            $currentWishlist = $currentUser->findWishlist($_SESSION['currentWishlist']);
            if ($currentWishlist)
            {
                $item = new Item($id, $name, $link, $rating, $price, 
                                $currentWishlist->id);
                $item->update(array('item' => $item));
                
                ItemsController::index();
            }
            else
            {
                WishlistsController::index();
            }
        }
        else
        {
            WishlistsController::index();
        }
    }

/*  **********************************************************************
    Function: destroy
    Description: Deletes the Item from the db if the User is authorized.
                 Redirects to the list of all the Wishlists's Items
    **********************************************************************  */
    public function destroy($item_id)
    {
        $currentUser = parent::authenticate();
        if (isset($_SESSION['currentWishlist']))
        {
            $currentWishlist = $currentUser->findWishlist($_SESSION['currentWishlist']);
            if ($currentWishlist)
            {
                $item = $currentWishlist->findItem($item_id);
                $item->destroy();

                // TODO: flash message 'item was successfully deleted'
                ItemsController::index();
            }
            else
            {
                WishlistsController::index();
            }
        }
        else
        {
            WishlistsController::index();
        }
    }

/*  **********************************************************************
    Function: parametersAreValid
    Description: Validates $_GET and $_POST variables to allow safe routing
    **********************************************************************  */
    public static function parametersAreValid($params = array())
    {
        foreach ($params as $key => $value)
        {
            switch ($key)
            {
                case 'id':
                    // integer, required
                    if (! is_numeric($value))
                    {
                        return false;
                    }
                    break;
                case 'name':
                    // string, required
                    if ($value === "")
                    {
                        return false;
                    }
                    break;
                case 'link':
                    // string, but not required 
                    break;
                case 'rating':
                    // single integer, but possibly should required
                    break;
                case 'price':
                    // numeric string, required
                    if (! is_numeric($value))
                    {
                        return false;
                    }
                    break;
                default:
                    // value is invalid (parameter not in whitelist)
                    return false;
            }
        }

        return true;
    }

    // check that an int won't overflow
    public static function intIsJustRight($integerValue)
    {
        return ($integerValue < PHP_INT_MAX) && ($integerValue >= 0);
    }
}

/*  ======================================================================
    Routing                 (& white listing TODO or black listing ? )
    ======================================================================  */
// form for editing a single item
if (isset($_GET['edit']) && isset($_GET['item_id']))
{
    if (ItemsController::parametersAreValid(array('id' => $_GET['item_id'])))
    {
        if (ItemsController::intIsJustRight($_GET['item_id']))
        {
            ItemsController::editForm($_GET['item_id']);
        }
        else
        {
            ItemsController::index();
        }
    }
    else
    {
        ItemsController::index();
    }
}
// delete an existing item
else if (isset($_GET['delete']) && isset($_GET['item_id']))
{
    if (ItemsController::parametersAreValid(array('id' => $_GET['item_id'])))
    {
        if (ItemsController::intIsJustRight($_GET['item_id']))
        {
            ItemsController::destroy($_GET['item_id']);
        }
        else
        {
            ItemsController::index();
        }
    }
    else
    {
        ItemsController::index();
    }
}
// show a single existing item
else if (isset($_GET['item_id']))
{
    if (ItemsController::parametersAreValid(array('id' => $_GET['item_id'])))
    {
        if (ItemsController::intIsJustRight($_GET['item_id']))
        {
            ItemsController::show($_GET['item_id']);
        }
        else
        {
            ItemsController::index();
        }
    }
    else
    {
        ItemsController::index();
    }
}
// update an existing item
else if (isset($_POST['update_item']))
{
    if (ItemsController::parametersAreValid(array(
        'name'  => $_POST['name'], 
        'link'  => $_POST['link'], 
        'rating'=> $_POST['rating'],
        'price' => $_POST['price'], 
        'id'    => $_POST['id'])))
    {
        // sanitize inputs
        $cleanName = trim($_POST['name']);
        $cleanLink = trim($_POST['link']);
        $cleanLink = filter_var($cleanLink, FILTER_SANITIZE_URL);
        // the above doesn't actually check to see if the url is valid
        // because we don't care that much

        $cleanRating = trim($_POST['rating']);
        if ($cleanRating === "")
        {
            // just use zero if rating is empty
            // - this won't be the case when using a select box instead
            $cleanRating = 0;
        }
        // ensure that rating is only a single digit character
        else if (preg_match("/^\d{1}$/", $cleanRating) !== 1)
        {
            // rating has not been formatted correctly
            // reload edit form
            ItemsController::editForm($_POST['id']);
        }

        $cleanPrice = trim($_POST['price']);
        if ((preg_match("/^\d{1,4}\.\d{0,2}$/", $cleanPrice) !== 1) &&
            (preg_match("/^\d{1,4}$/", $cleanPrice) !== 1))
        {
            // price has not been formatted correctly
            // easier to reload the edit form than to try to clean a price
            ItemsController::editForm($_POST['id']);
        }

        $cleanID = trim($_POST['id']);
        if (preg_match("/^\d{1,}$/", $cleanID) !== 1)
        {
            // $_POST['id'] has somehow been tampered with
            // reload clean form
            ItemsController::editForm($_POST['id']);
            
            // ...
            // OR: show off snidey "I caught you, lousy hacker" page
            // ...
        }

        ItemsController::update($cleanName, $cleanLink, $cleanRating, 
                                $cleanPrice, $cleanID);
    }
    else
    {
        // somehow tell the user what they did wrong (if js is disabled)
        // redirect to index, because $_POST['id'] could be tainted
        ItemsController::index();
    }
}
// create a new item
else if (isset($_POST['create_item']) && 
            isset($_POST['name']) && isset($_POST['price']))
{
    if (ItemsController::parametersAreValid(array(
        'name'  => $_POST['name'], 
        'link'  => $_POST['link'], 
        'rating'=> $_POST['rating'], 
        'price' => $_POST['price'])))
    {
        // TODO: SAME AS ABOVE !!! DRY !!!
        // TODO: side-note: fat models/skinny controllers 
        // sanitize inputs
        $cleanName = trim($_POST['name']);
        $cleanLink = trim($_POST['link']);
        $cleanLink = filter_var($cleanLink, FILTER_SANITIZE_URL);
        $cleanRating = trim($_POST['rating']);
        
        $cleanRating = trim($_POST['rating']);
        if ($cleanRating === "")
        {
            // just use zero if rating is empty
            // - this won't be the case when using a select box instead
            $cleanRating = 0;
        }
        // ensure that rating is only a single digit character
        else if (preg_match("/^\d{1}$/", $cleanRating) !== 1)
        {
            // rating has not been formatted correctly
            // reload new form
            ItemsController::newForm();
        }
        
        $cleanPrice = trim($_POST['price']);
        if ((preg_match("/^\d{1,4}\.\d{0,2}$/", $cleanPrice) !== 1) &&
            (preg_match("/^\d{1,4}$/", $cleanPrice) !== 1))
        {
            // price has not been formatted correctly (may mess with db)
            // easier to reload the new form than to try to clean a price
            ItemsController::newForm();
        }

        ItemsController::create($cleanName, $cleanLink, $cleanRating, $cleanPrice);
    }
    else
    {
        // somehow tell the user what they did wrong (if js is disabled)
        ItemsController::newForm();
    }
}
// form for a new item
else if (isset($_GET['new_item']))
{
    ItemsController::newForm();
}
else if (isset($_GET['item']))
{
    ItemsController::index(); 
}
?>
