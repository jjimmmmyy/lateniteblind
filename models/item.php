<?php
/*  ======================================================================
    File: LateNiteBlind/models/item.php
    Creator: James Jackson
    ======================================================================  */

include_once ROOT_PATH . '/db/db.php';

class Item
{
    public $id, $name, $link, $rating, $price, $wishlist_id;

/*  **********************************************************************
    Function: __construct
    Description: Creates a new Item.
    TODO: do we need to validate the args?
    **********************************************************************  */
    function __construct($id = NULL, $name = NULL, $link = NULL, $rating = NULL,
        $price = NULL, $wishlist_id = NULL)
    {
        $this->id = $id;
        $this->name = $name;
        $this->link = $link;
        $this->rating = $rating;
        $this->price = $price;
        $this->wishlist_id = $wishlist_id;
    }

/*  **********************************************************************
    Function: find
    Description: Searches the db for the Item by id
    Returns: an Item - which client must dereference, 
             or NULL? TODO see __construct
    **********************************************************************  */
    public static function find($item_id)
    {
        $db = new DB();

        $item_details = $db->query('SELECT * FROM items where id=?', 
                                    array($item_id));
        $item_details = $item_details[0];
        
        $db = NULL;

        $item = new Item(   $item_details['id'],
                            $item_details['name'],   
                            $item_details['link'],   
                            $item_details['rating'],   
                            $item_details['price'],
                            $item_details['wishlist_id']);   
        return $item;
    }

/*  **********************************************************************
    Function: all
    Description: Gets all the Items from the db
    Returns: array of Items - which client must dereference, 
             or NULL? TODO see __construct
    **********************************************************************  */
    public static function all()
    {
        $db = new DB();

        // grab all items
        $item_details = $db->query('SELECT * FROM items');
        
        $db = NULL;

        $items = array();
        $i = 0;
        foreach ($item_details as $item)
        {
            $items[$i] = new Item(  $item['id'], $item['name'], 
                                    $item['link'], $item['rating'], 
                                    $item['price'], $item['wishlist_id']);
            $i++;
        }
        
        return $items;
    }

/*  **********************************************************************
    Function: lastCreated
    Description: Gets the most recent Item added to the db
    Returns: an Item - which client must dereference, 
             or NULL? TODO see __construct
    **********************************************************************  */
    public static function lastCreated()
    {
        $db = new DB();

        $item_details = $db->query('SELECT * FROM items ORDER BY id DESC');
        $item_details = $item_details[0];
        
        $db = NULL;

        $item = new Item(   $item_details['id'],
                            $item_details['name'],   
                            $item_details['link'],   
                            $item_details['rating'],   
                            $item_details['price'],
                            $item_details['wishlist_id']);   

        return $item;
    }

/*  **********************************************************************
    Function: save
    Description: Saves this Item to the db
                 TODO no model validation before save
    **********************************************************************  */
    public function save()
    {
        $db = new DB();
        $db->query(
'INSERT INTO items(name, link, rating, price, wishlist_id) VALUES(?, ?, ?, ?, ?)',
            array($this->name, $this->link, $this->rating, $this->price, 
                    $this->wishlist_id));

        $db = NULL;
    }

/*  **********************************************************************
    Function: update
    Description: Updates this Item in the db
                 TODO no model validation before update
    **********************************************************************  */
    public function update($item)
    {
        extract($item);
        
        $db = new DB();
        $db->query(
'UPDATE items SET name=?, link=?, rating=?, price=? WHERE id=?',
            array($item->name, $item->link, $item->rating, $item->price, 
                    $item->id));

        $db = NULL;
    }

/*  **********************************************************************
    Function: destroy
    Description: Deletes this Item from the db
    **********************************************************************  */
    public function destroy()
    {
        $db = new DB();
        $db->query('DELETE FROM items WHERE id=?', array($this->id));

        $db = NULL;
    }
}
?>
