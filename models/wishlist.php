<?php
/*  ======================================================================
    File: LateNiteBlind/models/wishlist.php
    Creator: James Jackson
    ======================================================================  */

require_once ROOT_PATH . 'db/db.php';

class Wishlist
{
    public $id, $name, $user_id;

/*  **********************************************************************
    Function: __construct
    Description: Creates a new Wishlist.
    TODO: do we need to validate the args?
    **********************************************************************  */
    function __construct($id = NULL, $name = NULL, $user_id = NULL)
    {
        $this->id = $id;
        $this->name = $name;
        $this->user_id = $user_id;
    }
    
/*  **********************************************************************
    Function: find
    Description: Searches the db for the Wishlist by id
    Returns: a  Wishlist - which client must dereference, 
             or NULL - if no Wishlist was found
    **********************************************************************  */
    public static function find($wishlist_id)
    {
        $db = new DB();
        
        $wishlist_details = $db->query('SELECT * FROM wishlists WHERE id=?',
                                        array($wishlist_id));
        if ($wishlist_details)
        {
            $wishlist_details = $wishlist_details[0];

            $db = NULL;

            $wishlist = new Wishlist(   $wishlist_details['id'],
                                        $wishlist_details['name'],
                                        $wishlist_details['user_id']);
            return $wishlist;
        }
        else
        {
            return NULL;
        }
    }

/*  **********************************************************************
    Function: all
    Description: Gets all the Wishlists from the db
    Returns: array of Wishlists - which client must dereference, 
             or empty array TODO should this be NULL if empty? see __construct
    **********************************************************************  */
    public static function all()
    {
        $db = new DB();
        
        $wishlist_details = $db->query('SELECT * FROM wishlists');

        $db = NULL;

        $wishlists = array();
        $i = 0;
        foreach ($wishlist_details as $wishlist)
        {
            $wishlists[$i] = new Item(  $wishlist['id'], 
                                        $wishlist['name'],
                                        $wishlist['user_id']);
            $i++;
        }

        return $wishlists;
    }

/*  **********************************************************************
    Function: update
    Description: Updates this Wishlist in the db
                 TODO no model validation before update
    **********************************************************************  */
    public function update($wishlist)
    {
        extract($wishlist);

        $db = new DB();
        $db->query('UPDATE wishlists SET name=? WHERE id=?', 
                    array($wishlist->name, $wishlist->id));
        $db = NULL;
    }
    
/*  **********************************************************************
    Function: destroy
    Description: Deletes this Wishlist from the db
    **********************************************************************  */
    public function destroy()
    {
        $db = new DB();
        $db->query('DELETE FROM wishlists WHERE id=?', array($this->id));
        $db = NULL;
    }
     
/*  **********************************************************************
    Function: findItem
    Description: finds a single Item associated with this Wishlist
    Returns: an Item - which client must dereference,
             or NULL - if the Item could not be found
    **********************************************************************  */
    public function findItem($item_id)
    {
        $db = new DB();

        $item_details = $db->query(
        'SELECT * FROM items WHERE id=? AND wishlist_id=?', 
                                    array($item_id, $this->id));
        if ($item_details)
        {
            $item_details = $item_details[0];
            
            $db = NULL;

            $item = new Item(   $item_details['id'],
                                $item_details['name'],   
                                $item_details['link'],   
                                $item_details['rating'],   
                                $item_details['price'],
                                $item_details['wishlist_id']);   

            return $item;
        }
        else
        {
            return NULL;
        }
    }

/*  **********************************************************************
    Function: allItems
    Description: grabs all the Items associated with this Wishlist
    Returns: array of Items - which client must dereference,
            TODO or NULL?
    **********************************************************************  */
    public function allItems()
    {
        $db = new DB();

        // grab all items belonging to this wishlist
        $item_details = $db->query(
        'SELECT * FROM items WHERE wishlist_id=? ORDER BY price DESC', 
                                    array($this->id));
        
        $db = NULL;

        $items = array();
        $i = 0;
        foreach ($item_details as $item)
        {
            $items[$i] = new Item(  $item['id'], $item['name'], 
                                    $item['link'], $item['rating'], 
                                    $item['price'], $item['wishlist_id']);
            $i++;
        }
        
        return $items;
    }

/*  **********************************************************************
    Function: itemCount
    Description: counts the total Items associated with this Wishlist
    Returns: int
    **********************************************************************  */
    public function itemCount()
    {
        $db = new DB();

        // count all items belonging to this wishlist
        $item_count = $db->query(
        'SELECT * FROM items WHERE wishlist_id=?', 
            array($this->id));
        
        $db = NULL;

        return count($item_count);
    }

/*  **********************************************************************
    Function: save
    Description: Saves this Wishlist to the db
                 TODO model validation
    **********************************************************************  */
    public function save()
    {
        $db = new DB();
        $db->query('INSERT INTO wishlists(name, user_id) VALUES(?, ?)',
                    array($this->name, $this->user_id));
        $db = NULL;
    }
}
?>
