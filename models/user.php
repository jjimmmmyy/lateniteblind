<?php
/*  ======================================================================
    File: LateNiteBlind/models/user.php
    Creator: James Jackson
    ======================================================================  */

require_once ROOT_PATH . 'db/db.php';
require_once ROOT_PATH . 'third_party/PasswordHash.php';

class User
{
    public $id, $email, $password;

/*  **********************************************************************
    Function: __construct
    Description: Creates a new User.
    TODO: do we need to validate the args?
    **********************************************************************  */
    function __construct($id = NULL, $email = NULL, $password = NULL)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
    }

/*  **********************************************************************
    Function: find
    Description: Searches the db for the User by id
    Returns: a  User - which client must dereference, 
             or NULL - if no User was found
    **********************************************************************  */
    public static function find($user_id)
    {
        $db = new DB();
        
        // check if user with id exists in db
        $userDetails = 
            $db->query('SELECT * FROM users WHERE id=?', array($user_id)); 
        if (!$userDetails)
        {
            return NULL;
        }
        $userDetails = $userDetails[0];
        
        $db = NULL;

        // if so instanciate new user with these details
        $user = new User(   $userDetails['id'],
                            $userDetails['email'], 
                            $userDetails['hashed_password']); 
        return $user;
    }

/*  **********************************************************************
    Function: all
    Description: Gets all the Users from the db
    Returns: array of Users - which client must dereference, 
             or empty array TODO should this be NULL if empty? see __construct
    **********************************************************************  */
    public static function all()
    {
        $db = new DB();

        // grab all users
        $user_details = $db->query('SELECT * FROM users');
        
        $db = NULL;

        $users = array();
        $i = 0;
        foreach ($user_details as $user)
        {
            $users[$i] = new User(  $user['id'], $user['email'], 
                                    $user['hashed_password']);
            $i++;
        }
        
        return $users;
    }

/*  **********************************************************************
    Function: findWishlist
    Description: finds a single Wishlist associated with this User
    Returns: a Wishlist - which client must dereference,
             or NULL - if the Wishlist could not be found
    **********************************************************************  */
    public function findWishlist($wishlist_id)
    {
        $db = new DB();

        $wishlist_details = $db->query(
        'SELECT * FROM wishlists WHERE id=? AND user_id=?', 
                                    array($wishlist_id, $this->id));
        if ($wishlist_details)
        {
            $wishlist_details = $wishlist_details[0];
            
            $db = NULL;

            $wishlist = new Wishlist(   $wishlist_details['id'],
                                        $wishlist_details['name'],   
                                        $wishlist_details['user_id']);   

            return $wishlist;
        }
        else
        {
            return NULL;
        }
    }
    
/*  **********************************************************************
    Function: allWishlists
    Description: grabs all the Wishlists associated with this User
    Returns: array of Wishlists - which client must dereference,
            TODO or NULL?
    **********************************************************************  */
    public function allWishlists()
    {
        $db = new DB();
        
        // grab all wishlists belonging to this user
        $wishlist_details = $db->query(
        'SELECT * FROM wishlists WHERE user_id=?', array($this->id));
        
        $db = NULL;

        $wishlists = array();
        $i = 0;
        foreach ($wishlist_details as $wishlist)
        {
            $wishlists[$i] = new Wishlist(  $wishlist['id'], 
                                            $wishlist['name'], 
                                            $wishlist['user_id']);
            $i++;
        }
        
        return $wishlists;
    }
    
/*  **********************************************************************
    Function: hashedPassword
    Description: Performs 3rd-party hash on plaintext password string
    Returns: hashed password
    **********************************************************************  */
    private static function hashedPassword($password)
    {
    // these few lines could be in a config file instead
    //---------------------------
        // base-2 logarithm of the iteration count used for password stretching
        $hashCostLog2 = 8;
        // do we require the hashes to be portable to older systems? (less secure)?
        $hashIsPortable = false;
    //---------------------------
        
        $hasher = new PasswordHash($hashCostLog2, $hashIsPortable);
        $hash = $hasher->HashPassword($password);
        if (strlen($hash) < 20)
        {
            fail('Failed to hash new password');
        }
        unset($hasher);
        
        return $hash;
    }
    
/*  **********************************************************************
    Function: save
    Description: Saves this User to the db, hashing their password
                 TODO error handling when hashedPassword fails
    **********************************************************************  */
    public function save()
    {
        $hashedPassword = User::hashedPassword($this->password);

        if ($hashedPassword)
        {
            $db = new DB();
            $db->query(
                'INSERT INTO users(email, hashed_password) VALUES(?, ?)',
                array($this->email, $hashedPassword));
            
            $db = NULL;
        }
        else
        {
            // error handling!

            // password couldn't be hashed!

            // user couldn't be saved!
        }
    }
    
/*  **********************************************************************
    Function: authenticate
    Description: checks whether a User's email and password match our records
                 TODO differentiate between incorrect password and email
    Returns: authenticated User, 
             or NULL - if user email not found OR incorrect password
    **********************************************************************  */
    public static function authenticate($email, $password)
    {
        $db = new DB();
        
        // sanitize email 
        $cleanEmail = trim($email);
        $cleanEmail = filter_var($cleanEmail, FILTER_SANITIZE_EMAIL); 
        
        $user_details = 
            $db->query('SELECT * FROM users WHERE email=?', array($cleanEmail)); 
        
        $db = NULL;

        if ($user_details)
        {
            $user_details = $user_details[0];
            $user = new User(   $user_details['id'],
                                $user_details['email'], 
                                NULL);
             
            $hashCostLog2 = 8;
            $hashIsPortable = false;
            $hasher = new PasswordHash($hashCostLog2, $hashIsPortable);
            
            if ($hasher->CheckPassword($password, 
                $user_details['hashed_password']))
            {
                return $user;
            }
            else
            {
                return NULL;
            }
        }
        else
        {
            return NULL;
        }
    }
}
?>
